package it.polito.s197850.barcodealpha;

import android.animation.Animator;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import it.polito.s197850.barcodealpha.Db.Adapter;
import it.polito.s197850.barcodealpha.Utilities.Singleton;

public class listConferenze extends AppCompatActivity {
    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private ListView mListView;
    FloatingActionButton fab;
    private static final String SHOWCASE_ID = "conferenze_tutorial";
    private HashMap<String, String> map = new HashMap<>();
    int selectedPosition = 0;
    public Adapter DbAdapter = new Adapter(this);
    CustomAdapter adapter;
    ViewHolder holder;
    Boolean noRadioSelected = true;
    ArrayList<ItemList> mItemVector;
    String idConf;
    String idForm;


    private final int COLLAPSED_HEIGHT_1 = 150;

    private final int EXPANDED_HEIGHT_1 = 550;

    private boolean accordion = true;


    ImageView logo;
    Animator anim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_conferenze);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(noRadioSelected)
                    Snackbar.make(view, "Seleziona una conferenza.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                else {
                    Singleton.getInstance().setCodConferenza(idConf);
                    Singleton.getInstance().setIdForm(idForm);
                    finish();
                }
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ArrayList<ItemList> arrayList = new ArrayList<>();
        mListView = (ListView) findViewById(R.id.list_view);
        adapter = new CustomAdapter(this, arrayList);
        mListView.setAdapter(adapter);
        TextView title = (TextView)findViewById(R.id.title);
        int currCollapsedHeight = COLLAPSED_HEIGHT_1, currExpandedHeight = EXPANDED_HEIGHT_1;
        title.setText(Singleton.getInstance().getNomeSala() + ":");


        DbAdapter = new Adapter(this);
        DbAdapter.open();
        ItemToSend item = null;
        Cursor cursor = DbAdapter.getConferences(Singleton.getInstance().getIdSala());
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                int pos = cursor.getColumnIndex("codconferenza");
                String codConf = cursor.getString(pos);
                pos = cursor.getColumnIndex("conferenza");
                String conf = cursor.getString(pos);
                pos = cursor.getColumnIndex("OraInizio");
                String start = cursor.getString(pos);
                //pos = cursor.getColumnIndex("idForm");
                //String idForm = cursor.getString(pos);
                currCollapsedHeight = currCollapsedHeight + 50;
                currExpandedHeight = currExpandedHeight + 50;
                ItemList itemList = new ItemList(start,null, -1, "20", conf, codConf, COLLAPSED_HEIGHT_1, COLLAPSED_HEIGHT_1, EXPANDED_HEIGHT_1);
                arrayList.add(itemList);
                cursor.moveToNext();
            }
        }

        DbAdapter.close();
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                toggle(view, position);
            }
        });
    }


    class CustomAdapter extends ArrayAdapter<ItemList> {

        Context mContext;
        View row;

        private int selected = -1;


        public CustomAdapter(Context context, ArrayList<ItemList> list) {
            super(context, 0, list);
            mContext = context;
            mItemVector = list;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            row = convertView;

            final ItemList itemList = mItemVector.get(position);
            int finish = mItemVector.size();


            if (row == null) {
                row = LayoutInflater.from(getContext()).inflate(R.layout.list_row, parent, false);
                RelativeLayout rowWrap = (RelativeLayout) row.findViewById(R.id.text_wrap);
                holder = new ViewHolder(rowWrap, (TextView) row.findViewById(R.id.textView),
                        (ImageView) row.findViewById(R.id.imageView),
                        (RadioButton) row.findViewById(R.id.checkBox));
                holder.setViewWrap(rowWrap);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }


            holder.getViewWrap().setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, itemList.getCurrentHeight()));
            // holder.getText().setCompoundDrawablesWithIntrinsicBounds(itemList.getDrawable(), 0, 0, 0);
            final String conf = itemList.getConferenza();
            final String start = itemList.getStart();
            SpannableString spanString = new SpannableString(start);
            spanString.setSpan(new android.text.style.StyleSpan(Typeface.ITALIC), 0, start.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            if (conf != null) {
                holder.image.setImageResource(R.drawable.ic_event);
                holder.button.setTag(position);
                holder.button.setChecked(position == selected);
                holder.text.setText(String.format("%s \n %s %s", spanString, getResources().getString(R.string.ConferenceString), conf));
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noRadioSelected = false;
                        fab.setImageResource(R.drawable.ic_action_tick);
                        selectedPosition = (Integer) v.getTag();
                        selected = (Integer) v.getTag();
                        idConf = itemList.idConferenza;
                        idForm = itemList.idForm;
                        notifyDataSetChanged();
                        //mCallback.SendConference(itemList.conferenza, itemList.idConferenza);
                    }
                });
            }
            itemList.setHolder(holder);
            return row;
        }

    }

    private void toggle(View view, final int position) {
        ItemList listItem = mItemVector.get(position);
        listItem.getHolder().setRowWrap((RelativeLayout) view);

        int fromHeight = 0;
        int toHeight = 0;


        TextView t = (TextView) view.findViewById(R.id.textView);
        ImageView i = (ImageView) view.findViewById(R.id.imageView);
        Layout l = t.getLayout();
        int lines = l.getLineCount();
        if (l.getEllipsisCount(lines - 1) > 0) {
            if (listItem.isOpen()) {
                view.setVerticalScrollBarEnabled(true);
                fromHeight = listItem.getExpandedHeight();
                toHeight = listItem.getCollapsedHeight();
                t.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_wizard_size));
                t.setEllipsize(TextUtils.TruncateAt.END);
                t.setSingleLine(true);
            } else {
                fromHeight = listItem.getCollapsedHeight();
                toHeight = listItem.getExpandedHeight();
                t.setEllipsize(null);
                t.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_wizard_size_small));
                t.setSingleLine(false);


                // This closes all item before the selected one opens
                if (accordion) {
                    closeAll();
                }
            }

            toggleAnimation(listItem, position, fromHeight, toHeight, true);
        } else
            closeAll();
    }

    private void closeAll() {
        int i = 0;
        for (ItemList listItem : mItemVector) {
            if (listItem.isOpen()) {
                listItem.getHolder().getText().setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_wizard_size));
                listItem.getHolder().getText().setEllipsize(TextUtils.TruncateAt.END);
                listItem.getHolder().getText().setSingleLine(true);

                toggleAnimation(listItem, i, listItem.getExpandedHeight(),
                        listItem.getCollapsedHeight(), false);
            }
            i++;
        }
    }

    private void toggleAnimation(final ItemList listItem, final int position,
                                 final int fromHeight, final int toHeight, final boolean goToItem) {

        ResizeAnimation resizeAnimation = new ResizeAnimation(adapter,
                listItem, 0, fromHeight, 0, toHeight);
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                listItem.setOpen(!listItem.isOpen());
                listItem.setDrawable(listItem.isOpen() ? R.drawable.ic_store
                        : R.drawable.down);
                listItem.setCurrentHeight(toHeight);
                adapter.notifyDataSetChanged();

                if (goToItem)
                    goToItem(position);
            }
        });

        listItem.getHolder().getRowWrap().startAnimation(resizeAnimation);
    }

    private void goToItem(final int position) {
        mListView.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mListView.smoothScrollToPosition(position);
                } catch (Exception e) {
                    mListView.setSelection(position);
                }
            }
        });
    }

    public class ResizeAnimation extends Animation {
        private View mView;
        private float mToHeight;
        private float mFromHeight;

        private float mToWidth;
        private float mFromWidth;

        private CustomAdapter mListAdapter;
        private ItemList mListItem;

        public ResizeAnimation(CustomAdapter listAdapter, ItemList listItem,
                               float fromWidth, float fromHeight, float toWidth, float toHeight) {
            mToHeight = toHeight;
            mToWidth = toWidth;
            mFromHeight = fromHeight;
            mFromWidth = fromWidth;
            mView = listItem.getHolder().getRowWrap();
            mListAdapter = listAdapter;
            mListItem = listItem;
            setDuration(200);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float height = (mToHeight - mFromHeight) * interpolatedTime
                    + mFromHeight;
            float width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
            AbsListView.LayoutParams p = (AbsListView.LayoutParams) mView.getLayoutParams();
            p.height = (int) height;
            p.width = (int) width;
            mListItem.setCurrentHeight(p.height);
            mListAdapter.notifyDataSetChanged();
        }
    }
}
