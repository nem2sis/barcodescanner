package it.polito.s197850.barcodealpha;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.github.jorgecastilloprz.FABProgressCircle;
import com.github.jorgecastilloprz.listeners.FABProgressListener;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

import java.util.List;

import at.markushi.ui.CircleButton;
import it.polito.s197850.barcodealpha.Utilities.Singleton;

/**
 * This sample performs continuous scanning, displaying the barcode and source image whenever
 * a barcode is scanned.
 */
public class ContinuousCaptureActivity extends AppCompatActivity implements FABProgressListener,
        CompoundBarcodeView.TorchListener{
    private final String URL = "http://affidabilita.eu/wsaet/GestioneDatiVisitatore.asmx";
    private final String NAMESPACE = "http://atenainfo.it/serviziweb/";
    private final String METHOD_NAME = "RegistraPartecipante";
    private final String SOAP_ACTION = NAMESPACE + METHOD_NAME;
    boolean confirmation = false;

    ImageView switchFlashLight;
    TextView ch;
    FloatingActionButton fabSend;
    FloatingActionButton fabCancel;
    boolean flashOn = false;
    FABProgressCircle fabpro;
    private static final String TAG = ContinuousCaptureActivity.class.getSimpleName();
    private CompoundBarcodeView barcodeView;

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText() != null) {
                barcodeView.setStatusText("Scansionato:" + result.getText());
                Singleton.getInstance().setIdProgrForm(result.getText());
                Toast toast = Toast.makeText(getApplicationContext(),"Id Visitatore: " + result.getText() + '\n' +
                        "Premi i bottoni sotto per confermare/cancellare.", Toast.LENGTH_SHORT);
                toast.show();
                pause(barcodeView);

               /* okButton.setVisibility(View.VISIBLE);
                okButton.startAnimation(fab_open);
                cancelButton.setVisibility(View.VISIBLE);
                cancelButton.startAnimation(fab_open);*/

            }

        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };
    public void toggleOn(FloatingActionButton fab){
        Animation fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab.setVisibility(View.VISIBLE);
        fab.startAnimation(fab_open);
    }
    public void toggleOff(FloatingActionButton fab){
        Animation fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        fab.startAnimation(fab_close);
        fab.setVisibility(View.INVISIBLE);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.scan_activity);
        ch = (TextView)findViewById(R.id.conferenceHint);
        ch.setText("Conferenza: " + Singleton.getInstance().getCodConferenza());

        /*okButton = (CircleButton)findViewById(R.id.buttonOk);
        cancelButton = (CircleButton)findViewById(R.id.buttonCancel);
        okButton.setVisibility(View.INVISIBLE);
        cancelButton.setVisibility(View.INVISIBLE);*/
        fabSend = (FloatingActionButton)findViewById(R.id.fabSend);
        fabCancel = (FloatingActionButton)findViewById(R.id.fabCancel);
        switchFlashLight = (ImageView)findViewById(R.id.switch_flash);
        fabCancel.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#B71C1C")));
        fabSend.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#9E9E9E")));
        fabpro = (FABProgressCircle)findViewById(R.id.fabProgressCircle);
        fabpro.attachListener(this);
        if (!hasFlash()) {
            switchFlashLight.setVisibility(View.GONE);

        }
        fabSend.setVisibility(View.INVISIBLE);
        barcodeView = (CompoundBarcodeView) findViewById(R.id.barcode_scanner);
        barcodeView.setStatusText("Posiziona il Codice a Barre dentro il mirino.");
        barcodeView.decodeContinuous(callback);
        fabCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!fabSend.isShown()){
                    toggleOff(fabCancel);
                    toggleOff(fabSend);
                    finish();

                }
                resume(barcodeView);

            }
        });
        fabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fabpro.show();
                toggleOff(fabCancel);
                fabSend.setEnabled(false);
                new Task().execute();

            }
        });
       switchFlashLight.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if (!flashOn) {
                   barcodeView.setTorchOn();
                   flashOn = true;
                   switchFlashLight.setImageResource(R.drawable.ic_flash_off);
               } else {
                   barcodeView.setTorchOff();
                   flashOn = false;
                   switchFlashLight.setImageResource(R.drawable.ic_flash_on);
               }
           }
       });
    }

    private void sendData() {
        Toast.makeText(getApplicationContext(), "Tutto ok", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        barcodeView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        barcodeView.pause();
    }

    public void pause(View view) {
        barcodeView.pause();
        toggleOn(fabSend);
        fabpro.hide();
        fabpro.setVisibility(View.VISIBLE);
        toggleOn(fabCancel);

    }
    private boolean hasFlash() {
        boolean b = getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        return b;
    }

    public void resume(View view) {
        barcodeView.resume();
        barcodeView.setStatusText("Posiziona il Codice a Barre dentro il mirino.");
        fabSend.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#9E9E9E")));
        toggleOff(fabSend);
        if (!fabCancel.isShown()){
            toggleOn(fabCancel);
        }

    }

    public void triggerScan(View view) {
        barcodeView.decodeSingle(callback);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    public void onFABProgressAnimationEnd() {
        fabSend.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#4CAF50")));
        fabSend.setImageResource(R.drawable.ic_action_tick);
        fabSend.setEnabled(true);
        new CountDownTimer(50, 1000) {
            public void onFinish() {
                fabSend.setImageResource(R.drawable.ic_action_upload);
                resume(barcodeView);
            }

            public void onTick(long millisUntilFinished) {
                // millisUntilFinished    The amount of time until finished.
            }
        }.start();
    }

    @Override
    public void onTorchOn() {
        flashOn = true;

    }

    @Override
    public void onTorchOff() {
        flashOn = false;
        switchFlashLight.setImageResource(R.drawable.ic_flash_on);
    }


    class Task extends AsyncTask<String, Integer, Boolean> {


        @Override
        protected Boolean doInBackground(String... params) {
            sendToWebService();
            return confirmation;
        }

        @Override
        protected void onPreExecute() {
            // SHOW THE SPINNER WHILE LOADING FEEDS
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            fabSend.setEnabled(true);


        }

    }

    private boolean sendToWebService() {
        ContinuousCaptureActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Caricamento...", Toast.LENGTH_SHORT).show();
            }
        });
        int confirm = -99;
        SoapObject request = null, objMessages = null;
        SoapSerializationEnvelope envelope;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.implicitTypes = true;
        Element[] header = buildHeader();
        request = new SoapObject(NAMESPACE, METHOD_NAME);
        //request.addProperty("idProgrForm", "54544");

        PropertyInfo idProgrForm = new PropertyInfo();
        idProgrForm.setNamespace(NAMESPACE);
        idProgrForm.setName("idProgrForm");
        idProgrForm.setValue(Singleton.getInstance().getIdProgrForm());
        //idProgrForm.setValue("47");
        idProgrForm.setType(String.class);
        request.addProperty(idProgrForm);

        PropertyInfo codConf = new PropertyInfo();
        codConf.setNamespace(NAMESPACE);
        codConf.setName("codiceConf");
        codConf.setValue(Singleton.getInstance().getCodConferenza());
        codConf.setType(String.class);
        request.addProperty(codConf);

        PropertyInfo giorno = new PropertyInfo();
        giorno.setNamespace(NAMESPACE);
        giorno.setName("datagiorno");
        giorno.setValue(Singleton.getInstance().getGiorno());
        giorno.setType(String.class);
        request.addProperty(giorno);

        envelope.bodyOut = request;
        envelope.headerOut = header;
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug = true;

        try {
            //Get the response
            androidHttpTransport.call(SOAP_ACTION, envelope);
            Object retObj = envelope.bodyIn;
            SoapObject result = (SoapObject) retObj;
            if (result.getPropertyCount() > 0) {
                Object obj = result.getProperty(0);
                confirm = Integer.parseInt(result.getPropertyAsString(0));
            }
        }

        //Prende la risposta SOAP e ne estrae il corpo
        //SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
        //SoapObject datiVis = (SoapObject)envelope.getResponse();
        // ((TextView) this.findViewById(R.id.scanResult)).setText(String.format("Risultato dello scan: %1$s", datiVis.Cognome.toString()));
        //((TextView)this.findViewById(R.id.scanResult)).setText("dopo getresponse!");
        catch (Exception e) {
            // TODO: handle exception
            Log.d("Eccezione", String.valueOf(e.getStackTrace()));
            //((TextView) this.findViewById(R.id.scanResult)).setText(String.format("Risultato dello scan: %1$s", e.getStackTrace()));

        }
        /**if 0 NON PRENOTATO**/
        if (confirm >= 0) {
            if(confirm == 0){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MaterialDialog.Builder builder = new MaterialDialog.Builder(ContinuousCaptureActivity.this)
                                .title("Attenzione")
                                .titleColor((Color.RED))
                                .content(Html.fromHtml("Visitatore <b> NON PRENOTATO </b>"))
                                .neutralText("Va bene, Ho capito.")
                                .neutralColor(Color.RED)
                                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                    }
                                });
                        builder.show();
                    }
                });

            }
            ContinuousCaptureActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    fabpro.beginFinalAnimation();
                    Toast.makeText(getApplicationContext(), "Visitatore " + Singleton.getInstance().getIdProgrForm() + " inviato con successo!", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            ContinuousCaptureActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    fabpro.hide();
                    fabpro.setVisibility(View.INVISIBLE);
                    Toast.makeText(getApplicationContext(), "Errore collegamento al WebService.", Toast.LENGTH_LONG).show();
                    resume(barcodeView);
                }
            });

        }
        return confirmation;
    }


    private Element[] buildHeader() {
        Element[] header = new Element[1];
        header[0] = new Element().createElement(NAMESPACE, "Credenziali");
        Element username = new Element().createElement(NAMESPACE, "utente");
        username.addChild(Node.TEXT, "autorizzato");
        header[0].addChild(Node.ELEMENT, username);
        Element pass = new Element().createElement(NAMESPACE, "password");
        pass.addChild(Node.TEXT, "da-atena");
        header[0].addChild(Node.ELEMENT, pass);
        return header;
    }


}