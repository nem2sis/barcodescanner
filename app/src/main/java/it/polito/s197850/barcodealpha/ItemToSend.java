package it.polito.s197850.barcodealpha;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by marco on 19/10/2015.
 */
public class ItemToSend implements Parcelable {

    public String nome;
    public String cognome;
    public String idForm;
    public String idProgrForm;
    public String codConferenza;
    public String giorno;

    public ItemToSend(String idProgrForm, String codConferenza, String idForm, String giorno){
        super();
        this.idProgrForm = idProgrForm;
        this.codConferenza = codConferenza;
        this.idForm = idForm;
        this.giorno = giorno;

    }
    public ItemToSend(){
        this.nome = "<empty>";
        this.cognome = "<empty>";
        this.idForm = "<empty>";
        this.idProgrForm = "<empty>";
        this.codConferenza = "<empty>";
        this.giorno = "<empty>";
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public void setCodConferenza(String codConferenza) {
        this.codConferenza = codConferenza;
    }

    public void setGiorno(String giorno) {
        this.giorno = giorno;
    }

    public void setIdForm(String idForm) {
        this.idForm = idForm;
    }

    public void setIdProgrForm(String idProgrForm) {
        this.idProgrForm = idProgrForm;
    }

    public String getIdProgrForm() {
        return idProgrForm;
    }

    public String getIdForm() {
        return idForm;
    }

    public String getGiorno() {
        return giorno;
    }

    public String getCodConferenza() {
        return codConferenza;
    }



    protected ItemToSend(Parcel in) {
        idProgrForm = in.readString();
        codConferenza = in.readString();
        idForm = in.readString();
        giorno = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idProgrForm);
        dest.writeString(codConferenza);
        dest.writeString(idForm);
        dest.writeString(giorno);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ItemToSend> CREATOR = new Parcelable.Creator<ItemToSend>() {
        @Override
        public ItemToSend createFromParcel(Parcel in) {
            return new ItemToSend(in);
        }

        @Override
        public ItemToSend[] newArray(int size) {
            return new ItemToSend[size];
        }
    };
}