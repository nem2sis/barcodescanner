package it.polito.s197850.barcodealpha;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

/**
 * Created by marco on 09/10/2015.
 */
public abstract class Persona implements KvmSerializable {
    @Override
    public abstract Object getProperty(int arg0);

    @Override
    public abstract int getPropertyCount();

    @Override
    public abstract void getPropertyInfo(int index, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info);

    abstract String getInnerText();

    abstract void setInnerText(String s);

    @Override
    public abstract void setProperty(int arg0, Object arg1);
}
