package it.polito.s197850.barcodealpha.Db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by marco on 15/10/2015.
 */


public class Helper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME="db_sale.db";
    private static final int DATABASE_VERSION = 1;
    private  String DATABASE_PATH = null;
    private SQLiteDatabase myDataBase;
    private final Context myContext;
    private static Helper mInstance = null;

    private static final String DATABASE_CREATE = "CREATE TABLE conferenze (_id INTEGER DEFAULT 0 PRIMARY KEY AUTOINCREMENT, idSala INTEGER, NomeSala TEXT, Data TEXT, Conferenza TEXT, CodConferenza TEXT, OraInizio TEXT, OraFine TEXT)";

    public static Helper getInstance(Context context){
        if(mInstance == null)
            mInstance = new Helper(context.getApplicationContext());
        return mInstance;
    }
    private Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContext = context;
        DATABASE_PATH="/data/data/it.polito.s197850.barcodealpha/databases/";
        //AssetManager assetManager = context.getAssets();
    }
    /* Creates a empty database on the system and rewrites it with your own database.
            * */
    public void createDataBase() throws IOException {

        boolean dbExist = checkDataBase();

        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            myDataBase = this.getReadableDatabase();
            if(myDataBase.isOpen()) {
                myDataBase.close();
            }

            try {

                copyDataBase();

            } catch (IOException e) {

                throw new Error("Error copying database");

            }
        }

    }

    /**
     * Check if the database already exist to avoid re-copying the file each time you open the application.
     * @return true if it exists, false if it doesn't
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            String myPath = DATABASE_PATH + DATABASE_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }catch(SQLiteException e){

            //database does't exist yet.

        }

        if(checkDB != null){

            checkDB.close();

        }

        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     * */
    private void copyDataBase() throws IOException{

        //Open your local db as the input stream
        InputStream myInput = myContext.getAssets().open(DATABASE_NAME);

        // Path to the just created empty db
        String outFileName = DATABASE_PATH + DATABASE_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public SQLiteDatabase openDataBase() throws SQLException {

        //Open the database
        String myPath = DATABASE_PATH + DATABASE_NAME;
        return SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

    }

    @Override
    public synchronized void close() {
        super.close();
        if(myDataBase != null)
            myDataBase.close();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



}

