package it.polito.s197850.barcodealpha;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.leocardz.aelv.library.Aelv;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import it.polito.s197850.barcodealpha.Db.Adapter;
import it.polito.s197850.barcodealpha.Utilities.Singleton;

public class ListSale extends AppCompatActivity {
    ViewHolder holder;
    private static final String ARG_LAYOUT_RES_ID = "layoutResId";
    private ListView mListView;
    FloatingActionButton fab = null;
    private Map<String, Integer> map = new HashMap<>();
    int selectedPosition = 0;
    public Adapter DbAdapter = new Adapter(this);
    CustomAdapter adapter;
    Boolean noRadioSelected = true;
    String NomeSala;
    int idSala;
    ArrayList<ItemList> mItemVector;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    String date = df.format(Calendar.getInstance().getTime());
    private final int COLLAPSED_HEIGHT_1 = 150, COLLAPSED_HEIGHT_2 = 200,
            COLLAPSED_HEIGHT_3 = 250;

    private final int EXPANDED_HEIGHT_1 = 350, EXPANDED_HEIGHT_2 = 300,
            EXPANDED_HEIGHT_3 = 350, EXPANDED_HEIGHT_4 = 400;

    private boolean accordion = true;


    ImageView logo;
    Animator anim;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_sale);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent i = getIntent();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(noRadioSelected)
                    Snackbar.make(view, "Seleziona almeno una sala.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                else {
                    Singleton.getInstance().setIdSala(idSala);
                    Singleton.getInstance().setNomeSala(NomeSala);
                     finish();

                }
            }
        });

        mListView = (ListView) findViewById(R.id.list_view);
        ArrayList<ItemList> arrayList = new ArrayList<>();
        adapter = new CustomAdapter(this, arrayList);
        mListView.setAdapter(adapter);
        TextView title = (TextView)findViewById(R.id.title);
        title.setText(date + '\n' + "Sale Disponibili: ");
        int currCollapsedHeight = COLLAPSED_HEIGHT_1, currExpandedHeight = EXPANDED_HEIGHT_1;


        DbAdapter = new Adapter(this);
        DbAdapter = DbAdapter.open();
        Cursor cursor;
        cursor = DbAdapter.fetchAllSale();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    int pos = cursor.getColumnIndex("nomesala");
                    NomeSala = cursor.getString(pos);
                    pos = cursor.getColumnIndex("idsala");
                    idSala = cursor.getInt(pos);
                    currCollapsedHeight = currCollapsedHeight + 50;
                    currExpandedHeight = currExpandedHeight + 50;
                    ItemList item = new ItemList(null, NomeSala, idSala, null,  null, null, COLLAPSED_HEIGHT_1, COLLAPSED_HEIGHT_1, EXPANDED_HEIGHT_1);
                    arrayList.add(item);
                    map.put(NomeSala, idSala);
                    cursor.moveToNext();
                }
            }

            cursor.close();
            DbAdapter.close();

        }
        final Aelv aelv = new Aelv(true, 200, arrayList, mListView, adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                toggle(view, position);
            }
        });

    }

    class CustomAdapter extends ArrayAdapter<ItemList> {

        Context mContext;
        View row;

        private int selected = -1;


        public CustomAdapter(Context context, ArrayList<ItemList> list) {
            super(context, 0, list);
            mContext = context;
            mItemVector = list;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            row = convertView;

            final ItemList itemList = mItemVector.get(position);
            int finish = mItemVector.size();


            if (row == null) {
                row = LayoutInflater.from(getContext()).inflate(R.layout.list_row, parent, false);

                RelativeLayout rowWrap = (RelativeLayout) row.findViewById(R.id.text_wrap);
                holder = new ViewHolder(rowWrap, (TextView) row.findViewById(R.id.textView),
                        (ImageView) row.findViewById(R.id.imageView),
                        (RadioButton) row.findViewById(R.id.checkBox));
                holder.setViewWrap(rowWrap);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }


            holder.getViewWrap().setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, itemList.getCurrentHeight()));
            // holder.getText().setCompoundDrawablesWithIntrinsicBounds(itemList.getDrawable(), 0, 0, 0);
            final int id = itemList.getIdSala();
            final String Sala = itemList.getSala();
            if (Sala != null) {
                holder.image.setImageResource(R.drawable.ic_store);
                holder.button.setTag(position);
                holder.button.setChecked(position == selected);
                holder.text.setText(String.format("%s %s", getResources().getString(R.string.SalaString), Sala));
                holder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noRadioSelected = false;
                        fab.setImageResource(R.drawable.ic_action_tick);
                        selectedPosition = (Integer) v.getTag();
                        selected = (Integer) v.getTag();
                        notifyDataSetChanged();
                        idSala = id;
                        NomeSala = Sala;
                    }
                });
            }
            itemList.setHolder(holder);
            return row;
        }

    }

    private void toggle(View view, final int position) {
        ItemList listItem = mItemVector.get(position);
        listItem.getHolder().setRowWrap((RelativeLayout) view);

        int fromHeight = 0;
        int toHeight = 0;


        TextView t = (TextView) view.findViewById(R.id.textView);
        ImageView i = (ImageView) view.findViewById(R.id.imageView);
        Layout l = t.getLayout();
        int lines = l.getLineCount();
        if (l.getEllipsisCount(lines - 1) > 0) {
            if (listItem.isOpen()) {
                view.setVerticalScrollBarEnabled(true);
                fromHeight = listItem.getExpandedHeight();
                toHeight = listItem.getCollapsedHeight();
                t.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_wizard_size));
                t.setEllipsize(TextUtils.TruncateAt.END);
                t.setSingleLine(true);
            } else {
                fromHeight = listItem.getCollapsedHeight();
                toHeight = listItem.getExpandedHeight();
                t.setEllipsize(null);
                t.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_wizard_size_small));
                t.setSingleLine(false);


                // This closes all item before the selected one opens
                if (accordion) {
                    closeAll();
                }
            }

            toggleAnimation(listItem, position, fromHeight, toHeight, true);
        } else
            closeAll();
    }

    private void closeAll() {
        int i = 0;
        for (ItemList listItem : mItemVector) {
            if (listItem.isOpen()) {
                listItem.getHolder().getText().setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_wizard_size));
                listItem.getHolder().getText().setEllipsize(TextUtils.TruncateAt.END);
                listItem.getHolder().getText().setSingleLine(true);

                toggleAnimation(listItem, i, listItem.getExpandedHeight(),
                        listItem.getCollapsedHeight(), false);
            }
            i++;
        }
    }

    private void toggleAnimation(final ItemList listItem, final int position,
                                 final int fromHeight, final int toHeight, final boolean goToItem) {

        ResizeAnimation resizeAnimation = new ResizeAnimation(adapter,
                listItem, 0, fromHeight, 0, toHeight);
        resizeAnimation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                listItem.setOpen(!listItem.isOpen());
                listItem.setDrawable(listItem.isOpen() ? R.drawable.ic_store
                        : R.drawable.down);
                listItem.setCurrentHeight(toHeight);
                adapter.notifyDataSetChanged();

                if (goToItem)
                    goToItem(position);
            }
        });

        listItem.getHolder().getRowWrap().startAnimation(resizeAnimation);
    }

    private void goToItem(final int position) {
        mListView.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mListView.smoothScrollToPosition(position);
                } catch (Exception e) {
                    mListView.setSelection(position);
                }
            }
        });
    }

    public class ResizeAnimation extends Animation {
        private View mView;
        private float mToHeight;
        private float mFromHeight;

        private float mToWidth;
        private float mFromWidth;

        private CustomAdapter mListAdapter;
        private ItemList mListItem;

        public ResizeAnimation(CustomAdapter listAdapter, ItemList listItem,
                               float fromWidth, float fromHeight, float toWidth, float toHeight) {
            mToHeight = toHeight;
            mToWidth = toWidth;
            mFromHeight = fromHeight;
            mFromWidth = fromWidth;
            mView = listItem.getHolder().getRowWrap();
            mListAdapter = listAdapter;
            mListItem = listItem;
            setDuration(200);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float height = (mToHeight - mFromHeight) * interpolatedTime
                    + mFromHeight;
            float width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
            AbsListView.LayoutParams p = (AbsListView.LayoutParams) mView.getLayoutParams();
            p.height = (int) height;
            p.width = (int) width;
            mListItem.setCurrentHeight(p.height);
            mListAdapter.notifyDataSetChanged();
        }
    }
    public interface OnPageListenerSala {
        void SendSala(String name, int idSala);

    }
}
