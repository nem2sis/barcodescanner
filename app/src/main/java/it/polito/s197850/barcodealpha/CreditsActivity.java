package it.polito.s197850.barcodealpha;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CreditsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        TextView t = (TextView) findViewById(R.id.my_name);
        ImageView view = (ImageView)findViewById(R.id.logoCredits);
        t.setTypeface(Typeface.createFromAsset(this.getAssets(), "Roboto-Italic.ttf"));
        t.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Uri uri = Uri.parse("http://www.linkedin.com/in/marcodirosa"); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                return  true;
            }
        });
       view.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View v) {
               Uri uri = Uri.parse("http://atenainfo.it/"); // missing 'http://' will cause crashed
               Intent intent = new Intent(Intent.ACTION_VIEW, uri);
               startActivity(intent);
               return true;
           }
       });
    }
}
