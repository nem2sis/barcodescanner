package it.polito.s197850.barcodealpha.Utilities;

/**
 * Created by marco on 31/12/2015.
 */
public class Singleton {
    private static Singleton mInstance = null;

    private String nomeSala;
    private int idSala;
    private String idForm;
    private String idProgrForm;
    private String codConferenza;
    private String giorno;

    public static void reset(){
        mInstance = null;
    }
    private Singleton(){
        nomeSala = "<empty>";
        idSala = -1;
        idForm = "<empty>";
        idProgrForm = "<empty>";
        codConferenza = "<empty>";
        giorno = "<empty>";
    }

    public static Singleton getInstance(){
        if(mInstance == null)
        {
            mInstance = new Singleton();
        }
        return mInstance;
    }

    public String getNomeSala() {
        return nomeSala;
    }

    public void setNomeSala(String nomeSala) {
        this.nomeSala = nomeSala;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    public String getIdForm() {
        return idForm;
    }

    public void setIdForm(String idForm) {
        this.idForm = idForm;
    }

    public String getIdProgrForm() {
        return idProgrForm;
    }

    public void setIdProgrForm(String idProgrForm) {
        this.idProgrForm = idProgrForm;
    }

    public String getCodConferenza() {
        return codConferenza;
    }

    public void setCodConferenza(String codConferenza) {
        this.codConferenza = codConferenza;
    }

    public String getGiorno() {
        return giorno;
    }

    public void setGiorno(String giorno) {
        this.giorno = giorno;
    }
}
