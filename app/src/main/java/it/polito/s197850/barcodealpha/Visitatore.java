package it.polito.s197850.barcodealpha;

import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import java.util.Hashtable;

public class Visitatore extends Persona {

    public int idProgrForm;
    public int idForm;
    public String nome;
    public String cognome;
    public String azienda;
    public String barCode;
    public int pIN;
    public String email;
    public String dataCompilazione;
    public boolean preIscritto;
    public boolean iscrittoReception;
    public boolean registrato;
    public boolean espositore;
    public boolean vnormale;
    public boolean vsmart;
    public boolean elaborato;
    public String dataRegistrazione;
    public String invioBadge;

    public Visitatore(){}

    public Visitatore(SoapObject soapObject)
    {
        if (soapObject == null)
            return;
        if (soapObject.hasProperty("IdProgrForm"))
        {
            Object obj = soapObject.getProperty("IdProgrForm");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                idProgrForm = Integer.parseInt(j.toString());
            }else if (obj!= null && obj instanceof Number){
                idProgrForm = (Integer) obj;
            }
        }
        if (soapObject.hasProperty("IdForm"))
        {
            Object obj = soapObject.getProperty("IdForm");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                idForm = Integer.parseInt(j.toString());
            }else if (obj!= null && obj instanceof Number){
                idForm = (Integer) obj;
            }
        }
        if (soapObject.hasProperty("Nome"))
        {
            Object obj = soapObject.getProperty("Nome");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                nome = j.toString();
            }else if (obj!= null && obj instanceof String){
                nome = (String) obj;
            }
        }
        if (soapObject.hasProperty("Cognome"))
        {
            Object obj = soapObject.getProperty("Cognome");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                cognome = j.toString();
            }else if (obj!= null && obj instanceof String){
                cognome = (String) obj;
            }
        }
        if (soapObject.hasProperty("Azienda"))
        {
            Object obj = soapObject.getProperty("Azienda");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                azienda = j.toString();
            }else if (obj!= null && obj instanceof String){
                azienda = (String) obj;
            }
        }
        if (soapObject.hasProperty("BarCode"))
        {
            Object obj = soapObject.getProperty("BarCode");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                barCode = j.toString();
            }else if (obj!= null && obj instanceof String){
                barCode = (String) obj;
            }
        }
        if (soapObject.hasProperty("PIN"))
        {
            Object obj = soapObject.getProperty("PIN");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                pIN = Integer.parseInt(j.toString());
            }else if (obj!= null && obj instanceof Number){
                pIN = (Integer) obj;
            }
        }
        if (soapObject.hasProperty("Email"))
        {
            Object obj = soapObject.getProperty("Email");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                email = j.toString();
            }else if (obj!= null && obj instanceof String){
                email = (String) obj;
            }
        }
        if (soapObject.hasProperty("DataCompilazione"))
        {
            Object obj = soapObject.getProperty("DataCompilazione");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                dataCompilazione = j.toString();
            }else if (obj!= null && obj instanceof String){
                dataCompilazione = (String) obj;
            }
        }
        if (soapObject.hasProperty("PreIscritto"))
        {
            Object obj = soapObject.getProperty("PreIscritto");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                preIscritto = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                preIscritto = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("IscrittoReception"))
        {
            Object obj = soapObject.getProperty("IscrittoReception");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                iscrittoReception = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                iscrittoReception = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Registrato"))
        {
            Object obj = soapObject.getProperty("Registrato");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                registrato = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                registrato = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Espositore"))
        {
            Object obj = soapObject.getProperty("Espositore");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                espositore = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                espositore = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Vnormale"))
        {
            Object obj = soapObject.getProperty("Vnormale");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                vnormale = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                vnormale = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Vsmart"))
        {
            Object obj = soapObject.getProperty("Vsmart");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                vsmart = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                vsmart = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("Elaborato"))
        {
            Object obj = soapObject.getProperty("Elaborato");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                elaborato = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                elaborato = (Boolean) obj;
            }
        }
        if (soapObject.hasProperty("DataRegistrazione"))
        {
            Object obj = soapObject.getProperty("DataRegistrazione");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                dataRegistrazione = j.toString();
            }else if (obj!= null && obj instanceof String){
                dataRegistrazione = (String) obj;
            }
        }
        if (soapObject.hasProperty("InvioBadge"))
        {
            Object obj = soapObject.getProperty("InvioBadge");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                invioBadge = j.toString();
            }else if (obj!= null && obj instanceof String){
                invioBadge = (String) obj;
            }
        }
    }
    @Override
    public Object getProperty(int arg0) {
        switch(arg0){
            case 0:
                return idProgrForm;
            case 1:
                return idForm;
            case 2:
                return nome;
            case 3:
                return cognome;
            case 4:
                return azienda;
            case 5:
                return barCode;
            case 6:
                return pIN;
            case 7:
                return email;
            case 8:
                return dataCompilazione;
            case 9:
                return preIscritto;
            case 10:
                return iscrittoReception;
            case 11:
                return registrato;
            case 12:
                return espositore;
            case 13:
                return vnormale;
            case 14:
                return vsmart;
            case 15:
                return elaborato;
            case 16:
                return dataRegistrazione;
            case 17:
                return invioBadge;
        }
        return null;
    }

    @Override
    public int getPropertyCount() {
        return 18;
    }

    @Override
    public void getPropertyInfo(int index, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info) {
        switch(index){
            case 0:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "IdProgrForm";
                break;
            case 1:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "IdForm";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Nome";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Cognome";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Azienda";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "BarCode";
                break;
            case 6:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "PIN";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "Email";
                break;
            case 8:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "DataCompilazione";
                break;
            case 9:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "PreIscritto";
                break;
            case 10:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "IscrittoReception";
                break;
            case 11:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "Registrato";
                break;
            case 12:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "Espositore";
                break;
            case 13:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "Vnormale";
                break;
            case 14:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "Vsmart";
                break;
            case 15:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "Elaborato";
                break;
            case 16:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "DataRegistrazione";
                break;
            case 17:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "InvioBadge";
                break;
        }
    }

    @Override
    public String getInnerText() {
        return null;
    }


    @Override
    public void setInnerText(String s) {
    }


    @Override
    public void setProperty(int arg0, Object arg1) {
    }

}
