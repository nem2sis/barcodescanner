package it.polito.s197850.barcodealpha;

import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leocardz.aelv.library.AelvListViewHolder;

/**
 * Created by marco on 12/11/2015.
 */
public class ViewHolder extends AelvListViewHolder
{
    RelativeLayout rowWrap;
    ImageView image;
    TextView text;
    RadioButton button;


    public ImageView getImage() {
        return image;
    }

    public TextView getText() {
        return text;
    }

    public RadioButton getButton() {
        return button;
    }

    public RelativeLayout getRowWrap() {
        return rowWrap;
    }

    public void setRowWrap(RelativeLayout rowWrap) {
        this.rowWrap = rowWrap;
    }

    public ViewHolder(RelativeLayout rowWrap, TextView text, ImageView image, RadioButton button) {
        super();
        this.rowWrap = rowWrap;
        this.text = text;
        this.image = image;
        this.button = button;
    }
}