package it.polito.s197850.barcodealpha;

import com.leocardz.aelv.library.AelvListItem;

import it.polito.s197850.barcodealpha.R;
import it.polito.s197850.barcodealpha.ViewHolder;

/**
 * Created by marco on 12/11/2015.
 */
public class ItemList extends AelvListItem {

    String sala;
    String conferenza;
    String idConferenza;
    String start;
    String idForm;
    int idSala;
    private int collapsedHeight, currentHeight, expandedHeight;
    private ViewHolder holder;
    private boolean isOpen;

    private int drawable;

    public String getIdForm() {
        return idForm;
    }

    public void setIdForm(String idForm) {
        this.idForm = idForm;
    }

    public ItemList(String start, String nomeSala, int idSala, String idForm, String conferenza, String idConferenza, int collapsedHeight, int currentHeight, int expandedHeight) {
        super();
        this.start = start;
        this.sala = nomeSala;
        this.idSala = idSala;
        this.idForm = idForm;
        this.conferenza = conferenza;
        this.idConferenza = idConferenza;
        this.collapsedHeight = collapsedHeight;
        this.currentHeight = currentHeight;
        this.expandedHeight = expandedHeight;
        this.isOpen = false;
        this.drawable = R.drawable.ic_action_expand;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getSala() {
        return sala;
    }

    public int getIdSala() {
        return idSala;
    }

    public int getCollapsedHeight() {
        return collapsedHeight;
    }

    public void setCollapsedHeight(int collapsedHeight) {
        this.collapsedHeight = collapsedHeight;
    }

    public String getConferenza() {
        return conferenza;
    }

    public void setConferenza(String conferenza) {
        this.conferenza = conferenza;
    }

    public String getIdConferenza() {
        return idConferenza;
    }

    public void setIdConferenza(String idConferenza) {
        this.idConferenza = idConferenza;
    }

    public int getCurrentHeight() {
        return currentHeight;
    }

    public void setCurrentHeight(int currentHeight) {
        this.currentHeight = currentHeight;
    }

    public int getExpandedHeight() {
        return expandedHeight;
    }

    public void setExpandedHeight(int expandedHeight) {
        this.expandedHeight = expandedHeight;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public ViewHolder getHolder() {
        return holder;
    }

    public void setHolder(ViewHolder holder) {
        this.holder = holder;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }
}
