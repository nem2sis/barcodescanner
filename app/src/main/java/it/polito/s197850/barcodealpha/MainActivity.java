package it.polito.s197850.barcodealpha;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.kxml2.kdom.Element;
import org.kxml2.kdom.Node;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import at.markushi.ui.CircleButton;
import it.polito.s197850.barcodealpha.Utilities.Singleton;

public class MainActivity extends AppCompatActivity {
    private final String EMPTY_STRING = "<empty>";
    private final String URL = "http://www.atenainfo.it/wsatena/GestioneDatiVisitatore.asmx";
    private final String NAMESPACE = "http://atenainfo.it/serviziweb/";
    private final String METHOD_NAME = "RegistraNuovoPartecipante";
    private final String SOAP_ACTION = NAMESPACE + METHOD_NAME;


    int finalHeight, finalWidth;
    boolean complete = false;
    boolean confirmation = false;
    CircleButton cb;
    TextView hint;
    Toolbar toolbar;
    private View menu;
    private String result;
    private ItemToSend user = new ItemToSend();
    SharedPreferences prefs;
    SharedPreferences.Editor edit;
    String nomeSala;
    int idSala = -1;
    String idConf;
    String date;



    @Override
    protected void onResume() {
        super.onResume();
        if (Singleton.getInstance().getIdSala() != -1) {
            cb.setAlpha((float) 0.3);
            hint.setAlpha((float) 0.4);
            cb.setEnabled(false);
            hint.setText("Seleziona la conferenza.");
        }
        if (!(Singleton.getInstance().getCodConferenza().equals("<empty>"))) {
            cb.setColor(Color.parseColor("#FF9100"));
            cb.setEnabled(true);
            cb.setAlpha((float) 1);
            hint.setAlpha((float) 1);
            hint.setText("Premi per scansionare il Codice a Barre!");
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_credits){
            startActivity(new Intent(this, CreditsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (toolbar != null) {
            // 0 - title
            // 1 - options menu
            menu = toolbar.getChildAt(1);
        }
        final ImageView logo = (ImageView)findViewById(R.id.logoaet);
        final ViewTreeObserver logoTree = logo.getViewTreeObserver();
        logoTree.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                finalHeight = logo.getMeasuredHeight();
                finalWidth = logo.getMeasuredWidth();
                if(finalWidth < 300 || finalHeight < 300){
                    logo.setVisibility(View.INVISIBLE);
                }
                logo.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            final String[] permissions = new String[]{Manifest.permission.CAMERA};
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        cb = (CircleButton) findViewById(R.id.cb);
        if (cb != null) {
            cb.setEnabled(false);
        }
        cb.setAlpha((float) 0.3);
        //cb.setColor(Color.parseColor("#be4544"));
        hint = (TextView) findViewById(R.id.hint);
        hint.setTextColor(Color.parseColor("#476b6b"));
        hint.setText("Seleziona prima la sala e la conferenza.");
        Button b1 = (Button) findViewById(R.id.buttonSale);
        Button b2 = (Button) findViewById(R.id.buttonConf);
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ContinuousCaptureActivity.class);
                startActivity(i);
            }
        });

        if (b1 != null) {
            b1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent i = new Intent(MainActivity.this, ListSale.class);
                    DateFormat d = new SimpleDateFormat("dd");
                    date = d.format(Calendar.getInstance().getTime());
                    if(!(date.equals("20") || date.equals("21"))) {
                        new MaterialDialog.Builder(MainActivity.this)
                                .items("20", "21")
                                .title("Seleziona il giorno")
                                .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                                    @Override
                                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                                        DateFormat d = new SimpleDateFormat("MM/yyyy");
                                        date = d.format(Calendar.getInstance().getTime());
                                        date = text + "/" + date;
                                        Singleton.getInstance().setGiorno(date);
                                        startActivity(i);
                                        return true;
                                    }
                                })
                                .positiveText("ok")
                                .show();
                    }
                    else{
                        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                        String date = df.format(Calendar.getInstance().getTime());
                        Singleton.getInstance().setGiorno(date);
                        startActivity(i);
                    }


                }
            });
        }
        if (b2 != null) {
            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    idSala = Singleton.getInstance().getIdSala();
                    nomeSala = Singleton.getInstance().getNomeSala();
                    if (idSala != -1 && !(nomeSala.equals("<empty>"))) {
                        Intent i = new Intent(MainActivity.this, listConferenze.class);
                        startActivity(i);
                    } else {
                        adb.setTitle("Attenzione").setMessage("Selezionare prima la sala della conferenza.")
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(MainActivity.this, ListSale.class);
                                        startActivity(i);

                                    }
                                })
                                .setNegativeButton("Cancel", null).show();
                    }
                }
            });
        }
    }



}