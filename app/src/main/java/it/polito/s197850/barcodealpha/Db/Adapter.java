package it.polito.s197850.barcodealpha.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import it.polito.s197850.barcodealpha.Utilities.Singleton;

public class Adapter {
    @SuppressWarnings("unused")
    private static final String LOG_TAG = Adapter.class.getSimpleName();
    private static final String DATABASE_TABLE_CONFERENCES = "conferenze";


    private Context context;
    private static SQLiteDatabase database;
    private Helper dbHelper = null;


    public static final String KEY_IDSALA = "idSala";
    public static final String KEY_IDFORM = "idForm";
    public static final String KEY_NAME = "NomeSala";
    public static final String KEY_DATA = "Data";
    public static final String KEY_CONFERENCE_NAME = "Conferenza";
    public static final String KEY_CONFERENCE_CODE = "CodConferenza";
    public static final String KEY_START = "OraInizio";
    public static final String KEY_END = "OraFine";


    /*DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    String date = df.format(Calendar.getInstance().getTime());*/ /**Modificare nella versione finale **/





    public Adapter(Context context) {
        this.context = context;
    }

    public Adapter open() throws SQLException {
        Helper dbHelper = Helper.getInstance(this.context);

        try {

            dbHelper.createDataBase();

        } catch (IOException ioe) {

            throw new Error("Unable to create database");

        }

        try {

            dbHelper.openDataBase();

        }catch(SQLException sqle){

            throw sqle;

        }
        database = dbHelper.openDataBase();
        return this;
    }

    public void close() {
        if(dbHelper != null)
            dbHelper.close();

    }


    //create an object

    //fetch all contacts
    public Cursor fetchAllSale() {
        String date = Singleton.getInstance().getGiorno();
        return database.query(true, DATABASE_TABLE_CONFERENCES, new String[]{KEY_IDSALA, KEY_NAME}, KEY_DATA + "=" + "'" + date + "'", null, KEY_NAME, null, null, null);
    }


    public Cursor getConferences(int filter_idSala){
        String date = Singleton.getInstance().getGiorno();
        Cursor mCursor = database.query(true, DATABASE_TABLE_CONFERENCES, new String[]{KEY_START, KEY_CONFERENCE_NAME, KEY_CONFERENCE_CODE}, KEY_DATA + "=" + "'" + date + "'" + " and " + KEY_IDSALA + "="
                                            + "'" + String.valueOf(filter_idSala) + "'", null, null, null, KEY_START + " Asc", null);
        return mCursor;
    }
    }